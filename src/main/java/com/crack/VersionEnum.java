package com.crack;

/**
 * @author huangxd
 * @date 2020/3/12
 */
public enum VersionEnum {

    /**
     * 版本枚举
     */
    V2019_3_0("2019.3.0", "t3/common/lic/ag", "ac", "ag", "aS"),
    V2019_4_0("2019.4.0", "t3/common/lic/ag", "ac", "ag", "aS"),
    V2019_4_1("2019.4.1", "t3/common/lic/ag", "ac", "ag", "aS"),
    V2019_5_0("2019.5.0", "t3/common/lic/ag", "ac", "ag", "aS"),
    V2019_5_1("2019.5.1", "t3/common/lic/ag", "ac", "ag", "aS"),
    V2019_6_0("2019.6.0", "t3/common/lic/ag", "ac", "ag", "aS"),
    V2019_6_1("2019.6.1", "t3/common/lic/ag", "ac", "ag", "aS"),
    V2019_7_0("2019.7.0", "t3/common/lic/ag", "ac", "ag", "aS"),
    V2019_7_1("2019.7.1", "t3/common/lic/ag", "ac", "ag", "aS"),
    V2020_1_0("2020.1.0", "t3/common/lic/ag", "ac", "ag", "aS"),
    //部分方法名改变
    V2020_1_1("2020.1.1", "t3/common/lic/ag", "ad", "ah", "aS"),
    V2020_1_2("2020.1.2", "t3/common/lic/ag", "ad", "ah", "aS"),
    V2020_2_0("2020.2.0", "t3/common/lic/ag", "ad", "ah", "aS"),
    V2020_2_1("2020.2.1", "t3/common/lic/ag", "ad", "ah", "aS"),
    //类名,方法名改变
    V2020_3_0("2020.3.0", "t3/common/lic/ac", "aa", "ae", "aP"),
    V2020_3_1("2020.3.1", "t3/common/lic/ac", "aa", "ae", "aP"),
    V2020_4_0("2020.4.0", "t3/common/lic/g/a", "isExpired", "bS", "cD"),
    V2020_5_0("2020.5.0", "t3/common/lic/g/a", "isExpired", "bQ", "cB"),
    V2020_6_0("2020.6.0", "t3/common/lic/g/a", "ca", "ce", "cQ"),
    V2020_6_1("2020.6.1", "t3/common/lic/g/a", "ca", "ce", "cQ"),
    V2020_7_0("2020.7.0", "t3/common/lic/g/a", "cb", "cf", "cR"),
    V2020_7_1("2020.7.1", "t3/common/lic/g/a", "ce", "ci", "cU"),
    V2020_8_0("2020.8.0", "t3/common/lic/g/a", "cj", "cn", "cZ"),
    V2020_9_1("2020.9.1", "t3/common/lic/g/a", "ck", "co", "da"),
    V2020_9_2("2020.9.2", "t3/common/lic/g/a", "ck", "co", "da"),
    V2020_9_3("2020.9.3", "t3/common/lic/g/a", "cx", "cB", "dm"),
    V2020_10_0("2020.10.0", "t3/common/lic/g/a", "cx", "cB", "dm"),
    V2020_10_1("2020.10.1", "t3/common/lic/g/a", "cx", "cB", "dm"),
    DEFAULT("newest", "t3/common/lic/g/a", "cx", "cB", "dm");


    /**
     * 软件版本
     */
    private final String version;
    /**
     * 试用期类名
     */
    private final String className;
    /**
     * 判断是否过期方法名
     */
    private final String expireStatusMethod;
    /**
     * 剩余试用期天数方法名
     */
    private final String daysRemainingMethod;
    /**
     * 试用期天数方法名
     */
    private final String probationaryMethod;

    VersionEnum(String version, String className, String expireStatusMethod, String daysRemainingMethod, String probationaryMethod) {
        this.version = version;
        this.className = className;
        this.expireStatusMethod = expireStatusMethod;
        this.daysRemainingMethod = daysRemainingMethod;
        this.probationaryMethod = probationaryMethod;
    }

    public String getVersion() {
        return version;
    }

    public String getClassName() {
        return className;
    }

    public String getExpireStatusMethod() {
        return expireStatusMethod;
    }

    public String getDaysRemainingMethod() {
        return daysRemainingMethod;
    }

    public String getProbationaryMethod() {
        return probationaryMethod;
    }
}
