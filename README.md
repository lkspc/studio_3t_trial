# MongoDB Studio 3T 无限试用
## 有效版本

1. 2019.x.x
1. 最新2020.10.1
## 相关
* javaagent
* javassist
## 使用
1. maven 打包 `mvn package` （或 [下载](https://gitee.com/gslt/studio_3t_trial/releases) 最新版）
1. jar 包放置到 `D:\software\jar-agent\` 安装目录
1. 修改 `Studio 3T.vmoptions` 文件, 添加 `-javaagent` 及 jar 包路径
```
示例: -javaagent:D:\software\jar-agent\studio_3t_crack-2.4.jar
```
## 效果图
#### 效果图
![效果图](./image/result.png)
#### IDEA调试
![调试](./image/debug.png)

## 备注
```bash
日志输出控制台: 3t.logToConsole
日志输出等级: 3t.logLevel
```
