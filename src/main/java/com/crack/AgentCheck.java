package com.crack;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;

import java.lang.instrument.ClassFileTransformer;
import java.security.ProtectionDomain;

public class AgentCheck implements ClassFileTransformer {

    /**
     * 代理检查类路径
     */
    public static final String APP_CLASS_NAME = "t3/dataman/mongodb/app/Studio3TApp";

    @Override
    public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined, ProtectionDomain protectionDomain, byte[] classfileBuffer) {
        /**
         * 代理检查
         */
        try {
            if (className.equals(APP_CLASS_NAME)) {
                System.out.println("========== 代理检查 ==========");
                System.out.println(className);
                String loadName = className.replaceAll("/", ".");
                CtClass ctClass = ClassPool.getDefault().get(loadName);
                CtMethod ctMethod1 = ctClass.getDeclaredMethod("aO");
                ctMethod1.setBody("{return false;}");
                return ctClass.toBytecode();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return classfileBuffer;
    }
}
